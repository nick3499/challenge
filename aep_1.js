aep = a => {
  var p
  for (i = 0; i < a.length-1; i++) {
    if(p < a[i] * a[i+1] || p == undefined) {
      p = a[i] * a[i+1];
    }
  }
  return p
}

console.log(aep([3, 6, -2, -5, 7, 3]))       // 21
console.log(aep([-1, -2]))                   // 2
console.log(aep([5, 1, 2, 3, 1, 4]))         // 6
console.log(aep([1, 2, 3, 0]))               // 6
console.log(aep([9, 5, 10, 2, 24, -1, -48])) // 50
console.log(aep([5, 6, -4, 2, 3, 2, -23]))   // 30
console.log(aep([4, 1, 2, 3, 1, 5]))         // 6
console.log(aep([-23, 4, -3, 8, -12]))       // -12
console.log(aep([1, 0, 1, 0, 1000]))         // 0
