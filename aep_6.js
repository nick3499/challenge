aep = a => {
  x = a[0] * a[0+1]
  for(i = 1; i < a.length; i++) {
    y = a[i] * a[i+1]
    if (x < y) {
      x = y
    }
  }
  return x
}

console.log(aep([3, 6, -2, -5, 7, 3]))       // 21
console.log(aep([-1, -2]))                   // 2
console.log(aep([5, 1, 2, 3, 1, 4]))         // 6
console.log(aep([1, 2, 3, 0]))               // 6
console.log(aep([9, 5, 10, 2, 24, -1, -48])) // 50
console.log(aep([5, 6, -4, 2, 3, 2, -23]))   // 30
console.log(aep([4, 1, 2, 3, 1, 5]))         // 6
console.log(aep([-23, 4, -3, 8, -12]))       // -12
console.log(aep([1, 0, 1, 0, 1000]))         // 0
